package br.com.devmedia.projeto.controle_estoque;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AprendendoSpringGradle2Application {

	public static void main(String[] args) {
		SpringApplication.run(AprendendoSpringGradle2Application.class, args);
	}
}
