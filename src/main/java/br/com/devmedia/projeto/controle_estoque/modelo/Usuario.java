package br.com.devmedia.projeto.controle_estoque.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario {

	@Id
	@SequenceGenerator(name = "usuarioSeq", sequenceName = "usuario_sequence", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuarioSeq")
	private Integer id;

	@Column(name = "nome", nullable = false, length = 60)
	private String nome;

	@Column(name = "email", nullable = false, length = 30)
	private String email;

	@Column(name = "senha", nullable = false, length = 30)
	private String senha;

	@Column(name = "papel", nullable = false, length = 30)
	private String papel;

}
